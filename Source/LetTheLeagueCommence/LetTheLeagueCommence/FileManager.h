// This file contains the "FileManager" class defintion where file management are handled

#pragma once
#include <string>
#include <vector>

using namespace std;

class FileManager
{
	protected:
		static string folderPath;											// The path to the folder where all the related file is being keep
	public:
		FileManager();
		static string GetFolderPath();										// Get default folder path
		static bool SetFolderPath(string path);								// Set default folder path
		static vector<string> ReadFromFile(string fileName);				// Read a file and return a vector of string, where a string represent a line
		static bool WriteToFile(vector<string> data, string fileName);		// Take lines of string inputted into vector<string> and overwrite to a specific file, return false if error occured
		static bool IsExist(string fileName);								// Check if a file exist or not
};
