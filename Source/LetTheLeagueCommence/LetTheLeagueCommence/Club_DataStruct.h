// This file contains data struct that are related to club

#pragma once
#include <string>
#include <vector>
using namespace std;


// Data structs a club update notifncation/action
struct ClubUpdate
{
	int action;			// Represent club update action in a season, include add and delete (see constants.h)
	string clubName;	// Represent club name
};