#pragma once

/* Here contains error & information strings that is commonly used */
//const string DIRECTORY_NOT_EXISTS_ERROR = "[Error]: The inputted directory is not exists. Please try again.";

// Logs [Error]
const string INPUT_FILE_CANT_EXTRACTED_ERROR = "[Error]: Input data cant be extracted. Maybe the input file is not exist or it cant be read";
const string CANT_OPEN_FILE_ERROR = "[Error]: The file can't be opened.";
const string CANT_COPY_FILE_ERROR = "[Error]: The file can't be copied.";
const string SOURCE_FILE_NOT_EXIST_ERROR = "[Error]: The source file is not exist.";
const string CANT_EXTRACT_SEASON_AND_MODE_ERROR = "[Error]: Can't extract number of seasons or/and mode.";
const string CANT_EXTRACT_NUM_OF_CHANGES_ERROR = "[Error]: Can't extract number of club updates and/or player updates.";
const string NOT_ENOUGH_DATA_INPUTFILE_ERROR = "[Error]: Not enough data to read.";
const string CANT_EXTRACT_CLUBACTION_ERROR = "[Error]: Can't extract club update action.";
const string INVALID_ACTION_TYPE_ERROR = "[Error]: Cant determine type of action.";
const string CLUB_FILE_EMPTY_ERROR = "[Error]: Club file is empty";
const string PLAYER_FILE_EMPTY_ERROR = "[Error]: Player file is empty";
const string EXCEED_MAX_SIZE_FIX_LENGTH = "[Error]: In club file, the data size exceeds the maximum size of fix-length record. Please specify larger FIX_LENGTH_RECORD_SIZE and try again.";
const string CLUB_EXIST_ERROR = "[Error]: Club name is already exist - Ignore action.";
const string PLAYER_EXIST_ERROR = "[Error]: Player is already exist in club - Ignore action.";
const string NO_CLUB_TO_DELETE_ERROR = "[Error]: No club with the name specified is found in file.";
const string NO_PLAYER_TO_DELETE_ERROR = "[Error]: No player with the name specified is found in file.";
const string CLUB_NOT_PARTICIPATE_ERROR = "[Error]: Club not participate in this season: ";
const string NO_CLUB_FILE_FOR_DEFRAGMENT = "[Error]: No club file data found to defragment.";
const string NO_PLAYER_FILE_FOR_DEFRAGMENT = "[Error]: No player file data found to defragment: ";
const string ALGORITHMS_ERROR = "[Error]: The data is not as expected. There is something wrong with the algorithm.";
const string NOT_DELETED_RECORD_ERROR = "[Error]: This is not a deleted record.";

// Logs [Info]
const string FIRST_FIT_MODE_NOTIFICATION = "[Info]: First-fit mode is now activated.";
const string BEST_FIT_MODE_NOTIFICATION = "[Info]: Best-fit mode is now activated.";
const string SET_WORKING_DIR_SUCCESS_NOTIFICATION = "[Info]: Set new working directory successfully.";
const string EXECUTE_ACTIONS_NOTIFICATION = "[Info]: Begin reading and executing action";
const string READ_INPUT_DATA_COMPLETE_NOTIFICATION = "[Info]: Read raw input data and store in a structural order complete.";

// Logs [Warning]
const string CLEAR_ALL_DATA_WARNING_DESCRIPTION = "[Warning]: Do you want to clear all data?";
const string CLEAR_ALL_DATA_WARNING_TITLE = "[Warning]: Clear All Logs";
const string CONTINUE_NOTIFICATION = "[Warning]: The program will be continued, buy results may not be as expected.";
const string CLUBNAME_TOO_LONG_WARNING = "[Warning]: Club name exceeds maximum allowed size.";
const string IGNORE_WARNING = "[Warning]: The action will be ignored.";

// Constant log texts
const string PLS_TRY_AGAIN_STR = "Please check for errors and try again.";
const string SOURCE_FILE_DETAILS_STR = "\tSource: ";
const string DESTINATION_FILE_DETAILS_STR = "\tDestination: ";
const string SEASON_STR = "___Season ";
const string CLUB_STR = "Club ";
const string PLAYER_STR = "Player ";
const string ADD_ACTION_STR = "\t+ Add action: ";
const string DELETE_ACTION_STR = "\t- Delete action: ";
const string SPECIFY_FILE_PATH_NOTIFICATION = "Please specified directory that contains ";
const string ACTION_COMPLETED = "\t\t\tAction completed";
const string ACTION_FAILED = "\t\t\tAction failed";
const string LINE_ERROR_DETAILS_STR = "\tError on line ";
const string ERROR_FILE_DETAILS_STR = "\tError file: ";

/* Here contains constants about debugger and logs */
const int DEBUGGER_ERROR_LOG_ID = 0;
const int DEBUGGER_INFOR_LOG_ID = 1;
const int DEBUGGER_WARNI_LOG_ID = 2;
const int DEBUGGER_NORMA_LOG_ID = 3;
// Note: To change logs color, go to GUI.cpp line 75, 77

/* Here contains constants about input file data */
const int ADD_ACTION_ID = 1;
const int DELETE_ACTION_ID = 0;

const int FIRST_FIT_MODE_ID = 1;
const int BEST_FIT_MODE_ID = 0;

const string INPUT_FILENAME = "input.txt";
const string OUTPUT_CLUB_FILENAME = "clubs.txt";
const char PATH_SPLITTER = '\\' ;
const string OUTPUT_FILE_TYPE = ".txt";
const char INPUT_FILE_ARGUMENTS_SPLITTER = ' ';

const int MAX_SIZE_CLUBNAME = 3;
const int SEASON_AND_MODE_NUM_ARGUMENTS = 2;
const int NUM_ACTION_IN_SEASON_NUM_ARGUMENTS = 2;
const int CLUB_ACTION_NUM_ARGUMENTS = 2;
const int PLAYER_ACTION_NUM_ARGUMENTS = 3;

/* Here contains value about record */
const char DELETED_RECORD_IDENTIFIER = '*';
const char RECORD_SPLITTER = '|';
const string RECORD_SPLITTER_STR = "|";		// RECORD_SPLITTER_STR & RECORD_SPLITTER must be the same
const char NULL_VALUE_CHAR = '0';
const int FIX_LENGTH_RECORD_SIZE = 3;
const char EMPTY_SPACE_CHAR = '.';
const string RRN_COUNT_START = "0";

