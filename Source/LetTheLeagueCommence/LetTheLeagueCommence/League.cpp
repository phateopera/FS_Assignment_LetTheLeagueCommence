// This file contains funtions implementations for the class "League"

#include "League.h"
#include "Constants.h"
#include "FileManager.h"
#include "Tools.h"
#include "Debugger.h"
#include <fstream>
#include <algorithm>

League::League()
{
}

bool League::RetrieveDataFromFile(string inputFile)
{
	// Read data and store raw input
	vector<string> dataInput = FileManager::ReadFromFile(FileManager::GetFolderPath() + INPUT_FILENAME);
	int numOfSeasons = 0;
	bool isNotEnoughData = false;

	if (dataInput.size() == 0)		// Can't read data
	{
		Debugger::AppendLog(INPUT_FILE_CANT_EXTRACTED_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
		return false;
	}

	// Process raw data and convert to structural data
	int currLine = 0;
	// For the 1st line: extract number of season and mode
	try
	{
		vector<string> _temp = Tools::Split(dataInput[currLine], INPUT_FILE_ARGUMENTS_SPLITTER);	// Split arguments
		// Raise error if not enough arguments
		if (_temp.size() < SEASON_AND_MODE_NUM_ARGUMENTS)
		{
			Debugger::AppendLog(NOT_ENOUGH_DATA_INPUTFILE_ERROR, DEBUGGER_ERROR_LOG_ID);
			Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
			Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
			return false;
		}
		numOfSeasons = stoi(_temp[0]);
		mode = stoi(_temp[1]);

		// Set mode
		(mode == FIRST_FIT_MODE_ID) ? SetMode_FirstFit() : SetMode_BestFit();
	}
	catch (exception ex)
	{
		Debugger::AppendLog(CANT_EXTRACT_SEASON_AND_MODE_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
		return false;
	}
	currLine++;
	// For each season
	for (int currSeason = 0; currSeason < numOfSeasons; currSeason++)
	{
		Season* newSeason = new Season();
		int numClubChanges = 0;
		int numPlayerChanges = 0;
			
		// Extract number of club updates and player updates
		try
		{
			// Split arguments
			vector<string> _temp = Tools::Split(dataInput[currLine], ' ');	
			// Raise error if not enough arguments
			if (_temp.size() < NUM_ACTION_IN_SEASON_NUM_ARGUMENTS)
			{
				Debugger::AppendLog(NOT_ENOUGH_DATA_INPUTFILE_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
				return false;
			}
			numClubChanges = stoi(_temp[0]);
			numPlayerChanges = stoi(_temp[1]);
		}
		catch (exception ex)
		{
			Debugger::AppendLog(CANT_EXTRACT_NUM_OF_CHANGES_ERROR, DEBUGGER_ERROR_LOG_ID);
			Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
			Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);		
			return false;
		}
		currLine++;
		// For each club update actions
		for (int currClubAction = 0; currClubAction < numClubChanges; currClubAction++, currLine++)
		{
			// Check if there are enough line to read
			if (currLine > dataInput.size() - 1)
			{
				isNotEnoughData = true;
				break;
			}
			// Read action
			ClubUpdate* newClubUpdate = new ClubUpdate();
			try
			{
				vector<string> _temp = Tools::Split(dataInput[currLine], ' ');	// Split arguments
				// Raise error if not enough arguments
				if (_temp.size() < CLUB_ACTION_NUM_ARGUMENTS)
				{
					Debugger::AppendLog(NOT_ENOUGH_DATA_INPUTFILE_ERROR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
					return false;
				}
				newClubUpdate->action = stoi(_temp[0]);
				newClubUpdate->clubName = _temp[1];
			}
			catch (exception ex)
			{

				Debugger::AppendLog(CANT_EXTRACT_CLUBACTION_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_WARNI_LOG_ID);
				return false;
			}

			// Check if club name exceeds allowed chracter
			if (newClubUpdate->clubName.size() > MAX_SIZE_CLUBNAME)
			{
				Debugger::AppendLog(CLUBNAME_TOO_LONG_WARNING, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(IGNORE_WARNING, DEBUGGER_WARNI_LOG_ID);
			}

			// Add to data struct
			newSeason->clubUpdateActions.push_back(*newClubUpdate);
		}

		for (int currPlayerAction = 0; currPlayerAction < numPlayerChanges; currPlayerAction++, currLine++)
		{
			// Check if there are enough line to read
			if (currLine > dataInput.size() - 1)
			{
				isNotEnoughData = true;
				break;
			}

			// Read action
			PlayerUpdate* newPlayerUpdate = new PlayerUpdate();

			try
			{
				vector<string> _temp = Tools::Split(dataInput[currLine], ' ');		// Split arguments
				// Raise error if not enough arguments
				if (_temp.size() < PLAYER_ACTION_NUM_ARGUMENTS)
				{
					Debugger::AppendLog(NOT_ENOUGH_DATA_INPUTFILE_ERROR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
					return false;
				}
				newPlayerUpdate->action = stoi(_temp[0]);
				newPlayerUpdate->clubName = _temp[1];
				_temp.erase(_temp.begin(), _temp.begin() + 2);		// Delete first 2 character, therefore in _temp only name is left
				newPlayerUpdate->playerName = Tools::Join(_temp, ' ');		// Create name by join strings
			}
			catch (exception ex)
			{
				Debugger::AppendLog(CANT_EXTRACT_CLUBACTION_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(PLS_TRY_AGAIN_STR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(LINE_ERROR_DETAILS_STR + Tools::IntToString(currLine + 1), DEBUGGER_ERROR_LOG_ID);
				return false;
			}

			// Add to data struct
			newSeason->playerUpdateActions.push_back(*newPlayerUpdate);
		}

		// Add to data structs
		seasonUpdateActions.push_back(*newSeason);

		// Check if there are enough line to read
		if (isNotEnoughData)
			{
				Debugger::AppendLog(NOT_ENOUGH_DATA_INPUTFILE_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(CONTINUE_NOTIFICATION, DEBUGGER_WARNI_LOG_ID);
				break;
			}
	}	
	return true;
}

void League::SetMode_FirstFit()
{
	mode = FIRST_FIT_MODE_ID;
	Debugger::AppendLog(FIRST_FIT_MODE_NOTIFICATION, true);
}

void League::SetMode_BestFit()
{
	mode = BEST_FIT_MODE_ID;
	Debugger::AppendLog(BEST_FIT_MODE_NOTIFICATION, true);
}

bool League::AddClub(string clubName)
{
	// Check if data size is enough to be stored
	if (clubName.size() > FIX_LENGTH_RECORD_SIZE)
	{
		Debugger::AppendLog(EXCEED_MAX_SIZE_FIX_LENGTH, DEBUGGER_ERROR_LOG_ID);
		return false;
	}

	string clubName_modified = clubName;
	// Add empty character as end of string to specify empty space of a record
	for (int i = clubName_modified.size(); i < FIX_LENGTH_RECORD_SIZE; i++)
		clubName_modified += EMPTY_SPACE_CHAR;

	// If file is not exist
	if (!FileManager::IsExist(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME))
	{
		// Create file and write initial data
		string data = RRN_COUNT_START + RECORD_SPLITTER_STR + clubName_modified;
		vector<string> _temp;
		_temp.push_back(data);
		FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
		return true;
	}

	// Read data from file
	vector<string> _temp;
	_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
	if (_temp.size() == 0)	// No data found in club files
	{
		Debugger::AppendLog(CLUB_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
		return false;
	}
	string raw_data = _temp[0];	// Extract data since a file only have one line
	_temp.clear();
	
	// If club name is already exist
	if (raw_data.find(clubName) != string::npos)
	{
		Debugger::AppendLog(CLUB_EXIST_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	if (raw_data[0] == NULL_VALUE_CHAR)		// If there is no space need to be filled after deletion
	{
		//Add new record to end
		raw_data = raw_data + RECORD_SPLITTER_STR + clubName_modified;
	}
	else
	{
		vector<string> records = Tools::Split(raw_data, RECORD_SPLITTER);		// Extract records, each record is store in a string
		int positionToAdd = stoi(records[0]);			// Get first empty position
		if (records[positionToAdd].find(DELETED_RECORD_IDENTIFIER) == string::npos)		// If this is not a deleted record
		{
			Debugger::AppendLog(NOT_DELETED_RECORD_ERROR, DEBUGGER_ERROR_LOG_ID);
			Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
			return false;
		}
		records[0] = Tools::IntToString(stoi(records[positionToAdd].substr(1)));		// Update RRN before add
		records[positionToAdd] = clubName_modified;					// Add data to that location
		raw_data = Tools::Join(records, RECORD_SPLITTER);
	}

	// Save to file 
	_temp.push_back(raw_data);
	FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
	return true;
}

bool League::DeleteClub(string clubName)
{
	// If file is not exist
	if (!FileManager::IsExist(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME))
	{
		Debugger::AppendLog(CLUB_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	// Read data from file
	vector<string> _temp;
	_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
	if (_temp.size() == 0)	// No data found in club files
	{
		Debugger::AppendLog(CLUB_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
		return false;
	}
	string raw_data = _temp[0];
	_temp.clear();

	if (raw_data.find(clubName) == string::npos)		// If club name not exist in file
	{
		Debugger::AppendLog(NO_CLUB_TO_DELETE_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}
	vector<string> records = Tools::Split(raw_data, RECORD_SPLITTER);		// Extract records, each record is store in a string
	// Find location of record that need to be deleted
	for (int i = 1; i < records.size(); i++)
	{
		if (records[i].find(clubName) != string::npos)		// The record that need to be deleted
		{
			// Mark record as deleted
			records[i] = DELETED_RECORD_IDENTIFIER + records[0];

			// If free space exist --> Mark as free space using a character
			for (int k = records[i].size(); k < FIX_LENGTH_RECORD_SIZE; k++)
				records[i] += EMPTY_SPACE_CHAR;
			records[0] = Tools::IntToString(i);
			break;
		}	
	}
	
	// Join records into a single line
	raw_data = Tools::Join(records, RECORD_SPLITTER);
	
	// Save to file
	_temp.push_back(raw_data);
	FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
	return true;
}

bool League::AddPlayer(string playerName, string clubName, int season)
{
	// If file is not exist
	if (!FileManager::IsExist(FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE))
	{
		// Create file and write initial data
		string data = RRN_COUNT_START + RECORD_SPLITTER_STR + Tools::IntToString(playerName.size()) + playerName;
		vector<string> _temp;
		_temp.push_back(data);
		FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE);
		return true;
	}

	// Read data from file
	vector<string> _temp;
	_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE);
	if (_temp.size() == 0)	// No data found in player file
	{
		Debugger::AppendLog(PLAYER_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
		return false;
	}
	string raw_data = _temp[0];
	_temp.clear();

	// If player name is already exist
	if (raw_data.find(playerName) != string::npos)
	{
		Debugger::AppendLog(PLAYER_EXIST_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	// Extract records, each record is stored as a string
	vector<string> records = Tools::Split(raw_data, RECORD_SPLITTER);

	if (stoi(records[0]) == 0)		// If there is no space need to be filled after deletion
	{
		//Add new record to end
		raw_data = raw_data + RECORD_SPLITTER_STR + Tools::IntToString(playerName.size()) + playerName;
	}
	else
	{
		int lastRRN = 0;		// The RRN of the deleted record that contains the currRRN
		int currRRN = stoi(records[0]);		// The RRN of the current record that we analyze
		int currDeletedRecord = Helper_FindRecord(records, currRRN);		// The record index in "records" according to currRRN
		// Find suitable record to add data 
		if (mode == FIRST_FIT_MODE_ID)
		{
			bool isFound = false;		// Flag whether a suitable deleted record has been found
			while (currDeletedRecord != -1)		// Whether all deleted records has been searched or there is a error
			{
				// Get record size
				string recordData = records[currDeletedRecord];
				int _recordSize = stoi(recordData);

				// To be sure: Check if this is a deleted record
				int deleteRecordIdenifierPos = recordData.find(DELETED_RECORD_IDENTIFIER);
				if (deleteRecordIdenifierPos == string::npos)
				{
					Debugger::AppendLog(NOT_DELETED_RECORD_ERROR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
					return false;
				}

				if (_recordSize >= playerName.size())		// Deleted record has enough space to store new value
				{
					isFound = true;			// Mark suitable is found
					currRRN = stoi(recordData.substr(deleteRecordIdenifierPos + 1));		// Update the deleted record RRN (to use later)
					break;
				}

				// Not suitable record --> Extract new RRN & move to next deleted record
				lastRRN = currRRN;
				currRRN = stoi(recordData.substr(deleteRecordIdenifierPos + 1));
				currDeletedRecord = Helper_FindRecord(records, currRRN);
			} 

			if (isFound)		// Suitable position found
			{
				int _size = stoi(records[currDeletedRecord]);	// Get suitable record size
				// Create new record data
				string newData = playerName;
				// For the remaining space, add a special character indicate free space
				for (int i = playerName.size(); i < _size; i++)
					newData += EMPTY_SPACE_CHAR;
				newData = Tools::IntToString(_size) + newData;
				records[currDeletedRecord] = newData;		// Apply change to record

				// Update RRN of last record
				if (lastRRN == 0)		// No last record
					records[0] = Tools::IntToString(currRRN);		// Update to head RRN
				else
				{
					
					int lastRecordIdx = Helper_FindRecord(records, lastRRN);		// Move to the last deleted record
					string _lastRecordData = records[lastRecordIdx];		// Extract data
					int _size = stoi(_lastRecordData);						// Get size
					
					// Update new RRN for this deleted record
					string _newRecordData = DELETED_RECORD_IDENTIFIER + Tools::IntToString(currRRN);
					for (int i = _newRecordData.size(); i < _size; i++)
						_newRecordData += EMPTY_SPACE_CHAR;

					records[lastRecordIdx] = Tools::IntToString(_size) + _newRecordData;	// Apply changes
				}
			}
			else		// No suitable record found (due to error or record size is not enough)
			{
				// Add data to end of file
				string newData = Tools::IntToString(playerName.size()) + playerName;
				records.push_back(newData);
			}
		}
		else if (mode == BEST_FIT_MODE_ID)
		{
			int bestSize = INT_MAX;			// The current best record size
			int currBestRRN = 0;			// The current best record RRN
			int lastBestRRN = 0;			// The record RRN that contains the currBestRRN
			int nextBestRRN = 0;			// The RRN in the currBestRRN
			while (currDeletedRecord != -1)		// Whether all deleted records has been searched or there is a error
			{
				// Extract data from record
				string recordData = records[currDeletedRecord];
				int _recordSize = stoi(recordData);

				// To be sure: Check if this is a deleted record
				int deleteRecordIdenifierPos = recordData.find(DELETED_RECORD_IDENTIFIER);
				if (deleteRecordIdenifierPos == string::npos)
				{
					Debugger::AppendLog(NOT_DELETED_RECORD_ERROR, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
					return false;
				}

				if (_recordSize >= playerName.size() && _recordSize < bestSize)		// Deleted record has enough space to store new value and is better than the last best record
				{
					// Update best record information for later use
					bestSize = _recordSize;
					currBestRRN = currRRN;
					lastBestRRN = lastRRN;
					nextBestRRN = stoi(recordData.substr(deleteRecordIdenifierPos + 1));
				}

				// Extract new RRN & move to next deleted RRN
				lastRRN = currRRN;
				currRRN = stoi(recordData.substr(deleteRecordIdenifierPos + 1));
				currDeletedRecord = Helper_FindRecord(records, currRRN);
			}

			if (currBestRRN != 0)		// A suitable location is found
			{
				currDeletedRecord = Helper_FindRecord(records, currBestRRN);		// Find best record index in "records" given currBestRRN
				// Extract data from best RRN
				int _size = stoi(records[currDeletedRecord]);		

				// Create new record data
				string newData = playerName;
				// For the remaining space, add a special character indicate free space
				for (int i = playerName.size(); i < _size; i++)
					newData += EMPTY_SPACE_CHAR;
				newData = Tools::IntToString(_size) + newData;
				records[currDeletedRecord] = newData;	// Apply change to deleted record

				// Update RRN of last record
				if (lastBestRRN == 0)		// No last record
					records[0] = Tools::IntToString(nextBestRRN);		// Update to head RRN
				else
				{
					int lastRecordIdx = Helper_FindRecord(records, lastBestRRN);		// Move to last deleted record
					// Extract data
					string _lastRecordData = records[lastRecordIdx];			
					int _size = stoi(_lastRecordData);

					// Update new RRN
					string _newRecordData = DELETED_RECORD_IDENTIFIER + Tools::IntToString(nextBestRRN);
					for (int i = _newRecordData.size(); i < _size; i++)
						_newRecordData += EMPTY_SPACE_CHAR;

					// Apply changes
					records[lastRecordIdx] = Tools::IntToString(_size) + _newRecordData;
				}
			}
			else		// If no suitable record is found (due to error or not enough space)
			{
				// Add data to end of file
				string newData = Tools::IntToString(playerName.size()) + playerName;
				records.push_back(newData);
			}
		}

		raw_data = Tools::Join(records, RECORD_SPLITTER);	// Join records into a single string
	}

	// Save to file 
	_temp.push_back(raw_data);
	FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE);
	return true;
}

bool League::DeletePlayer(string playerName, string clubName, int season)
{
	// If file is not exist
	if (!FileManager::IsExist(FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE))
	{
		Debugger::AppendLog(PLAYER_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	// Read data from file
	vector<string> _temp;
	_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE);
	if (_temp.size() == 0)	// No data found in player file
	{
		Debugger::AppendLog(PLAYER_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		Debugger::AppendLog(ALGORITHMS_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}
	string raw_data = _temp[0];
	_temp.clear();

	// Find location of record that need to be deleted
	vector<string> records = Tools::Split(raw_data, RECORD_SPLITTER);		// Extract records, each record is stored into a string

	if (raw_data.find(playerName) == string::npos)		// If player doesnt exist in file
	{
		Debugger::AppendLog(NO_PLAYER_TO_DELETE_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	// For each record
	int deletePos = 1;		// The new RRN that need to be rewrite into head RRN after a record is deleted
	for (int i = 1; i < records.size(); i++)
	{
		if (records[i].find(playerName) != string::npos)		// If this is the record needed to delete
		{
			int _tempSize = stoi(records[i]);		// Get record size
			string newRecordValue = DELETED_RECORD_IDENTIFIER + records[0];		// Mark data as deleted
			// For the remaining space, using a special character to mark as deleted
			for (int k = newRecordValue.size(); k < _tempSize; k++)
				newRecordValue += EMPTY_SPACE_CHAR;
			records[i] = Tools::IntToString(_tempSize) + newRecordValue;	// Apply change to record
			break;
		}
		deletePos += stoi(records[i]);		// Update RRN
	}
	// Update the head RRN
	records[0] = Tools::IntToString(deletePos);

	// Save to file
	raw_data = Tools::Join(records, RECORD_SPLITTER);
	_temp.push_back(raw_data);
	FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + clubName + OUTPUT_FILE_TYPE);
	
	return true;
}

int League::Helper_FindRecord(vector<string> records, int rrn)
{
	if (rrn == 0)
		return -1;		// Not exist

	int currRRN = 1;
	for (int i = 1; i < records.size(); i++)
	{
		if (currRRN < rrn)		// Not there yet
			currRRN += stoi(records[i]);
		else
			return i;
	}
	return -1;
}

bool League::Defragment_ClubFile()
{
	// If file is not exist
	if (!FileManager::IsExist(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME))
	{
		Debugger::AppendLog(NO_CLUB_FILE_FOR_DEFRAGMENT, DEBUGGER_ERROR_LOG_ID);
		return true;
	}

	// Read data from file
	vector<string> _temp;
	_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);
	if (_temp.size() == 0)	// No data found in club files
	{
		Debugger::AppendLog(CLUB_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
		return true;
	}
	string raw_data = _temp[0];
	_temp.clear();

	vector<string> records = Tools::Split(raw_data, RECORD_SPLITTER);		// Extract records, each record is store in a string

	// Remove deleted space
	records[0] = RRN_COUNT_START;		// Reset RRN

	for (int i = 1; i < records.size(); i++)
	{
		if (records[i].find(DELETED_RECORD_IDENTIFIER) != string::npos)		// If this is a deleted record 
			records.erase(records.begin() + i);
	}

	// Wrire new data
	raw_data = Tools::Join(records, RECORD_SPLITTER);
	_temp.push_back(raw_data);
	FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + OUTPUT_CLUB_FILENAME);

	return true;
}

bool League::Defragment_PlayerFiles(vector<string> clubInSeason)
{
	for (int currClubIdx = 0; currClubIdx < clubInSeason.size(); currClubIdx++)
	{
		// If file is not exist
		if (!FileManager::IsExist(FileManager::GetFolderPath() + clubInSeason[currClubIdx] + OUTPUT_FILE_TYPE))
		{
			Debugger::AppendLog(NO_PLAYER_FILE_FOR_DEFRAGMENT + clubInSeason[currClubIdx], DEBUGGER_ERROR_LOG_ID);
			return true;
		}

		// Read data from file
		vector<string> _temp;
		_temp = FileManager::ReadFromFile(FileManager::GetFolderPath() + clubInSeason[currClubIdx] + OUTPUT_FILE_TYPE);
		if (_temp.size() == 0)	// No data found in club files
		{
			Debugger::AppendLog(CLUB_FILE_EMPTY_ERROR, DEBUGGER_ERROR_LOG_ID);
			return true;
		}
		string data = _temp[0];
		_temp.clear();
		
		vector<string> records = Tools::Split(data, RECORD_SPLITTER);

		// Defragment
		records[0] = RRN_COUNT_START;	// Restart RRN head
		for (int i = 1; i < records.size(); i++)
		{
			if (records[i].find(DELETED_RECORD_IDENTIFIER) != string::npos)		// If this is a deleted record 
			{
				records.erase(records.begin() + i);
				continue;
			}
	
			// Compact free space
			records[i].erase(remove(records[i].begin(), records[i].end(), EMPTY_SPACE_CHAR), records[i].end());

			// Update new size
			int size = stoi(records[i]);
			int lengthOfSizeString = Tools::IntToString(size).size();		// Get length of the number contains the record size
			records[i].erase(0, lengthOfSizeString);			// Remove old size
			records[i] = Tools::IntToString(records[i].size()) + records[i];
		}

		// Wrire new data
		data = Tools::Join(records, RECORD_SPLITTER);
		_temp.push_back(data);
		FileManager::WriteToFile(_temp, FileManager::GetFolderPath() + clubInSeason[currClubIdx] + OUTPUT_FILE_TYPE);
	}

	return true;
}

bool League::ClonePlayerFile(vector<string> clubInSeason, int season)
{
	bool failure = false;
	for (int i = 0; i < clubInSeason.size(); i++)
	{
		string sourcePath = FileManager::GetFolderPath() + clubInSeason[i] + OUTPUT_FILE_TYPE;
		string desPath = FileManager::GetFolderPath() + clubInSeason[i] + '.' + Tools::IntToString(season) + OUTPUT_FILE_TYPE;
		// TODO: Check for source existence
		bool isSuccess = Tools::CopyFile(sourcePath, desPath);
		if (!isSuccess)
			failure = true;
	}
	return !failure;
}

int League::Execute()
{
	vector<string> allClubs;		// Store all club name that participate in league
	vector<string> currClubs;		// Clubs that participate in a season (will be updated throughout seasons)

	// Open file input from the working folder & read
	bool isInputReadSuccess = RetrieveDataFromFile(FileManager::GetFolderPath() + INPUT_FILENAME);
	if (!isInputReadSuccess)		// Input file is not exist or cant be read
		return 0;
	Debugger::AppendLog(READ_INPUT_DATA_COMPLETE_NOTIFICATION, DEBUGGER_INFOR_LOG_ID);

	// Execute actions
	Debugger::AppendLog(EXECUTE_ACTIONS_NOTIFICATION, DEBUGGER_INFOR_LOG_ID);
	// For each season
	for (int currSeasonIdx = 0, numSeason = seasonUpdateActions.size(); currSeasonIdx < numSeason; currSeasonIdx++)
	{
		Debugger::AppendLog(SEASON_STR + Tools::IntToString(currSeasonIdx + 1), DEBUGGER_NORMA_LOG_ID);
		// For each club action
		for (int currClubActionIdx = 0, numClubActions = seasonUpdateActions[currSeasonIdx].clubUpdateActions.size(); currClubActionIdx < numClubActions; currClubActionIdx++)
		{
			// Get club update action on the current season
			ClubUpdate currUpdateAction = seasonUpdateActions[currSeasonIdx].clubUpdateActions[currClubActionIdx];
			if (currUpdateAction.action == ADD_ACTION_ID)		// If it is an add action 
			{
				// Add club to file
				Debugger::AppendLog(ADD_ACTION_STR + CLUB_STR + currUpdateAction.clubName, DEBUGGER_NORMA_LOG_ID);
				bool isSuccess = AddClub(currUpdateAction.clubName);
				if (!isSuccess)
				{
					Debugger::AppendLog(ACTION_FAILED, DEBUGGER_ERROR_LOG_ID);
					return EXIT_FAILURE;
				}
				else
					Debugger::AppendLog(ACTION_COMPLETED, DEBUGGER_INFOR_LOG_ID);
					

				// Store club information
				if (find(allClubs.begin(), allClubs.end(), currUpdateAction.clubName) == allClubs.end())	// If this is a new club
					allClubs.push_back(currUpdateAction.clubName);
				if (find(currClubs.begin(), currClubs.end(), currUpdateAction.clubName) == currClubs.end())	// If this club join a season and is not stored
					currClubs.push_back(currUpdateAction.clubName);

			}
			else if (currUpdateAction.action == DELETE_ACTION_ID)	// If it is an delete action
			{
				// Delete club from file
				Debugger::AppendLog(DELETE_ACTION_STR + CLUB_STR + currUpdateAction.clubName, DEBUGGER_NORMA_LOG_ID);
				bool isSuccess = DeleteClub(currUpdateAction.clubName);
				if (!isSuccess)
				{
					Debugger::AppendLog(ACTION_FAILED, DEBUGGER_ERROR_LOG_ID);
					return EXIT_FAILURE;
				}
				else
					Debugger::AppendLog(ACTION_COMPLETED, DEBUGGER_INFOR_LOG_ID);

				// Remove club from currClubs
				currClubs.erase(remove(currClubs.begin(), currClubs.end(), currUpdateAction.clubName), currClubs.end());
			}
			else
			{
				// Can't read type of action --> Error
				Debugger::AppendLog(INVALID_ACTION_TYPE_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(IGNORE_WARNING, DEBUGGER_WARNI_LOG_ID);
			}
		}

		// For each player action 

		for (int currPlayerActionIdx = 0, numPlayerActions = seasonUpdateActions[currSeasonIdx].playerUpdateActions.size(); currPlayerActionIdx < numPlayerActions; currPlayerActionIdx++)
		{
			// Get player update action on the current season
			PlayerUpdate currUpdateAction = seasonUpdateActions[currSeasonIdx].playerUpdateActions[currPlayerActionIdx];
			
			if (currUpdateAction.action == ADD_ACTION_ID)		// If it is an add action 
			{
				// Add player to file
				Debugger::AppendLog(ADD_ACTION_STR + CLUB_STR + currUpdateAction.clubName + " - " + PLAYER_STR + currUpdateAction.playerName, DEBUGGER_NORMA_LOG_ID);

				// Check whether club participate in this season
				if ((find(currClubs.begin(), currClubs.end(), currUpdateAction.clubName) == currClubs.end()))
				{
					// Club not participate in this season --> Ignore action
					Debugger::AppendLog(CLUB_NOT_PARTICIPATE_ERROR + currUpdateAction.clubName, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(IGNORE_WARNING, DEBUGGER_WARNI_LOG_ID);
					continue;
				}

				bool isSuccess = AddPlayer(currUpdateAction.playerName, currUpdateAction.clubName, currSeasonIdx + 1);
				if (!isSuccess)
				{
					Debugger::AppendLog(ACTION_FAILED, DEBUGGER_ERROR_LOG_ID);
					return EXIT_FAILURE;
				}
				else
					Debugger::AppendLog(ACTION_COMPLETED, DEBUGGER_INFOR_LOG_ID);
			}
			
			else if (currUpdateAction.action == DELETE_ACTION_ID)	// If it is an delete action
			{
				// Delete player from file
				Debugger::AppendLog(DELETE_ACTION_STR + CLUB_STR + currUpdateAction.clubName + " - " + PLAYER_STR + currUpdateAction.playerName, DEBUGGER_NORMA_LOG_ID);

				// Check whether club participate in this season
				if ((find(currClubs.begin(), currClubs.end(), currUpdateAction.clubName) == currClubs.end()))
				{
					// Club not participate in this season --> Ignore action
					Debugger::AppendLog(CLUB_NOT_PARTICIPATE_ERROR + currUpdateAction.clubName, DEBUGGER_ERROR_LOG_ID);
					Debugger::AppendLog(IGNORE_WARNING, DEBUGGER_WARNI_LOG_ID);
					continue;
				}

				bool isSuccess = DeletePlayer(currUpdateAction.playerName, currUpdateAction.clubName, currSeasonIdx + 1);
				if (!isSuccess)
				{
					Debugger::AppendLog(ACTION_FAILED, DEBUGGER_ERROR_LOG_ID);
					return EXIT_FAILURE;
				}
				else
					Debugger::AppendLog(ACTION_COMPLETED, DEBUGGER_INFOR_LOG_ID);
			}
			else
			{
				// Can't read type of action --> Error
				Debugger::AppendLog(INVALID_ACTION_TYPE_ERROR, DEBUGGER_ERROR_LOG_ID);
				Debugger::AppendLog(IGNORE_WARNING, DEBUGGER_WARNI_LOG_ID);
			}
		}
		
		// At the end of the season
		Defragment_PlayerFiles(currClubs);						// Defragment player files
		ClonePlayerFile(currClubs ,currSeasonIdx + 1);	// Clone club file
	}

	Defragment_ClubFile();	// Defragment club file after season end
	return EXIT_SUCCESS;
}