// This file contains data struct that are related to season

#pragma once
#include <string>
#include <vector>
#include "Club_DataStruct.h"
#include "Player_DataStruct.h"

using namespace std;

// Data structs that consist of actions within a specific season
struct Season
{
	vector<ClubUpdate> clubUpdateActions;			// List of club update actions
	vector<PlayerUpdate> playerUpdateActions;		// List of player update actions in a club that are in the season
};
